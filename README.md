# SAT gitlab stats

A very simple script to get a number of commits per project in a group

Example usage:

`./python3 count-commits.py -t <gitlab token> -b '2022-07-11' -e '2023-08-10' -g 'sat-mtl/metalab'`

Documentation:

```bash
usage: count-commits [-h] -t --token TOKEN -b BEGIN_DATE -e END_DATE [-g GROUP]

counts commits in a gitlab group

options:
  -h, --help            show this help message and exit
  -t --token TOKEN      your gtilab API token
  -b BEGIN_DATE, --begin-date BEGIN_DATE
                        begin date of the range, in the format YYYY-MM-DD
  -e END_DATE, --end-date END_DATE
                        end date of the range, in the format YYYY-MM-DD
  -g GROUP, --group GROUP
                        group name, deafults to sat-mtl/metalab
```

## Installation

You need to create an `API token` by going to your GitLab's **Preferences->Access Tokens**.
Then:

```bash
pip3 install python-gitlab
```
And you should be good to go.
