#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime

parser = argparse.ArgumentParser(
                    prog='count-commits',
                    description='counts commits in a gitlab group',
                    epilog='use at your own risk')

parser.add_argument('-t ''--token', type=str,
                    required=True,
                    dest='token',
                    help="your gtilab API token")
parser.add_argument('-b', '--begin-date',
                    required=True,
                    type=str,
                    dest='begin_date',
                    help='begin date of the range, in the format YYYY-MM-DD')
parser.add_argument('-e', '--end-date', type=str,
                    dest='end_date',
                    required=True,
                    help='end date of the range, in the format YYYY-MM-DD')

parser.add_argument('-g', '--group', type=str,
                    dest='group',
                    default='sat-mtl/metalab',
                    help='group name, deafults to sat-mtl/metalab')

args = parser.parse_args()

since_date = datetime.strptime(args.begin_date, '%Y-%m-%d')
until_date = datetime.strptime(args.end_date, '%Y-%m-%d')

gl = gitlab.Gitlab('https://gitlab.com', private_token=args.token)
group = gl.groups.get(args.group)
projects = group.projects.list(all=True)

for project in projects:
    manageable_project = gl.projects.get(project.id, lazy=False)
    commits = manageable_project.commits.list(all=True, since=since_date, until=until_date)
    number_of_commits = len(commits)
    if number_of_commits > 0:
        print(f"Project: {manageable_project.name}, Commits: {number_of_commits}")
